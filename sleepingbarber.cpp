#include "sleepingbarber.h"

#include <QDebug>
#include <QThread>
#include <QSemaphore>

const int COUNT_CUSTOMER = 3;

int customerEnabled(0); // Посетители
int barbersCount(1); // Парикмахер

bool stB = false;

bool flag[2];
int threadRun = 0;



SleepingBarber::SleepingBarber() :
    m_threadState(true),
    m_speedBarber(1),
    m_speedCustomer(1)
{

}

bool SleepingBarber::state() const
{
    return m_state;
}

bool SleepingBarber::threadState() const
{
    return m_threadState;
}

int SleepingBarber::speedBarber() const
{
    return m_speedBarber;
}

int SleepingBarber::speedCustomer() const
{
    return m_speedCustomer;
}




void SleepingBarber::barber() {

    while (m_threadState){
        QThread::sleep(3);
        flag[0] = true;
        qDebug() << m_threadState;
        while(flag[1] && m_threadState) {
            if (threadRun == 1) {
                flag[0] = false;
                while (threadRun == 1);
                flag[0] = true;
            }
        }

        // start Critivcal Section
        if (customerEnabled != 0){
            emit emitbarberChanged(1);
            customerEnabled--;
            emit emitCustomerChanged(customerEnabled);
            QThread::sleep(1);
            qDebug() << "Отработал клиента, клиетов в очереди: " << customerEnabled;
            emit emitbarberChanged(2);
        } else {
            qDebug() << "Спит";
            emit emitbarberChanged(0);
        }

        threadRun = 1;
        flag[0] = false;
        // end Critical Section
    }
}

void SleepingBarber::customer() {

    while (m_threadState){
        QThread::sleep(m_speedCustomer);
        emit emitCustomerChanged(customerEnabled);

        flag[1] = true;
        while(flag[0] && m_threadState) {

            if (threadRun == 0) {
                flag[1] = false;
                while (threadRun == 0);

                flag[1] = true;
            }
        }
        // start Critivcal Section
        QThread::sleep(m_speedCustomer);
        qDebug() << m_threadState;
        emit emitCustomerStateChanged(true);
        if (customerEnabled < COUNT_CUSTOMER) {
            qDebug() << "Новый посетитель";



            customerEnabled++;
            emit emitCustomerChanged(customerEnabled);


            qDebug() << "Barber начал работу";

        }else{
            qDebug() << "Посетитель ушёл!";
            emit emitCustomerStateChanged(false);
        }

        threadRun = 0;
        flag[1] = false;
    }
}



void SleepingBarber::setState(bool state)
{
    if (m_state == state)
        return;

    m_state = state;
    emit stateChanged(m_state);
}

void SleepingBarber::setThreadState(bool threadState)
{
    if (m_threadState == threadState)
        return;

    m_threadState = threadState;
    emit threadStateChanged(m_threadState);
}

void SleepingBarber::setSpeedBarber(int speedBarber)
{
    if (m_speedBarber == speedBarber)
        return;

    m_speedBarber = speedBarber / 4;
    emit speedBarberChanged(m_speedBarber);
}

void SleepingBarber::setSpeedCustomer(int speedCustomer)
{
    if (m_speedCustomer == speedCustomer)
        return;

    m_speedCustomer = speedCustomer / 4;
    emit speedCustomerChanged(m_speedCustomer);
}
