#ifndef SLEEPINGBARBER_H
#define SLEEPINGBARBER_H

#include <QObject>


class SleepingBarber : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool state READ state WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(bool threadState READ threadState WRITE setThreadState NOTIFY threadStateChanged)
    Q_PROPERTY(int speedBarber READ speedBarber WRITE setSpeedBarber NOTIFY speedBarberChanged)
    Q_PROPERTY(int speedCustomer READ speedCustomer WRITE setSpeedCustomer NOTIFY speedCustomerChanged)

public:
    SleepingBarber();

bool state() const;

bool threadState() const;

int speedBarber() const;

int speedCustomer() const;

public slots:
    void barber();

    void customer();

    void setState(bool state);

    void setThreadState(bool threadState);

    void setSpeedBarber(int speedBarber);

    void setSpeedCustomer(int speedCustomer);

signals:
    void emitCustomerStateChanged(bool);

    void emitCustomerChanged(int);

    void emitbarberChanged(int);

//    void emitCustomerHideShow();


    void stateChanged(bool state);

    void threadStateChanged(bool threadState);

    void speedBarberChanged(int speedBarber);

    void speedCustomerChanged(int speedCustomer);

private:
    bool m_state;


    bool m_threadState;
    int m_speedBarber;
    int m_speedCustomer;
};

#endif // SLEEPINGBARBER_H
