#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <sleepingbarber.h>
#include <QPixmap>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void updateBarber(int);

    void updateCustomer(int);

    void updateCustomerStarted(bool);

//    void hideLabelCustomerState();

    void on_customerSlider_valueChanged(int value);

    void on_barberSlider_valueChanged(int value);

private:
    Ui::MainWindow *ui;
    QThread *barberThread;
    QThread *customerThread;
    SleepingBarber *barberTask;
    SleepingBarber *customerTask;
    QPixmap barberImage;
    QPixmap stulImage;
    QPixmap customerImage;
    QPixmap customerStateImage;
    QPixmap customerExitImage;
    QPixmap barberSleep;
    QPixmap barberWork;
    QPixmap barberCompl;

    QIcon searchIcon;
};
#endif // MAINWINDOW_H
