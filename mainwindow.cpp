#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <sleepingbarber.h>

#include <QDebug>
#include <QPixmap>
#include <QThread>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap barberImage("./barber.png");
    QPixmap stulImage("./stul.png");
    QPixmap customerImage("./p.jpg");
    QPixmap barberSleep("./sleep.png");

    ui->barberState->setPixmap(barberSleep.scaled(50, 50, Qt::KeepAspectRatio));
    ui->st1->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
    ui->st2->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
    ui->st3->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
    ui->barberImage->setPixmap(barberImage.scaled(100, 100, Qt::KeepAspectRatio));


    ui->customerSlider->setEnabled(false);
    ui->barberSlider->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::updateCustomer(int state) {

//}

void MainWindow::on_pushButton_clicked()
{
    if (ui->pushButton->text() == "Start") {

        barberThread = new QThread();
        customerThread = new QThread();
        barberTask = new SleepingBarber;
        customerTask = new SleepingBarber;

        customerTask->moveToThread(customerThread);
        barberTask->moveToThread(barberThread);
        ui->pushButton->setText("Stop");
        connect(customerTask, SIGNAL(emitCustomerChanged(int)), this, SLOT(updateCustomer(int)));
        connect(customerTask, SIGNAL(emitCustomerStateChanged(bool)), this, SLOT(updateCustomerStarted(bool)));
        connect(barberTask, SIGNAL(emitCustomerChanged(int)), this, SLOT(updateCustomer(int)));
        connect(barberTask, SIGNAL(emitbarberChanged(int)), this, SLOT(updateBarber(int)));
        connect(barberThread, SIGNAL(started()), barberTask, SLOT(barber()));
        connect(customerThread, SIGNAL(started()), customerTask, SLOT(customer()));
        qDebug() << "Test";
        barberThread->start();
        customerThread->start();
        customerTask->setThreadState(true);
        barberTask->setThreadState(true);
        ui->customerSlider->setEnabled(true);
//        ui->barberSlider->setEnabled(true);
    }else {
        barberTask->setThreadState(false);
        customerTask->setThreadState(false);
        qDebug() << ui->pushButton->text();
        ui->customerSlider->setEnabled(false);
        ui->barberSlider->setEnabled(false);
        ui->pushButton->setText("Start");
    }
}


void MainWindow::updateCustomer(int status){
    qDebug() << status;
    QPixmap barberImage("./barber.png");
    QPixmap stulImage("./stul.png");
    QPixmap customerImage("./p.jpg");
    ui->listWidget->addItem("Занято " + QString::number(status) + "мест");
    switch (status) {
    case 0:
        ui->st1->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st2->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st3->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
        break;
    case 1:
        ui->st1->setPixmap(customerImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st2->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st3->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
        break;
    case 2:
        ui->st1->setPixmap(customerImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st2->setPixmap(customerImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st3->setPixmap(stulImage.scaled(100, 100, Qt::KeepAspectRatio));
        break;

    case 3:
        ui->st1->setPixmap(customerImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st2->setPixmap(customerImage.scaled(100, 100, Qt::KeepAspectRatio));
        ui->st3->setPixmap(customerImage.scaled(100, 100, Qt::KeepAspectRatio));
        break;
    default:
        break;
    }
}

void MainWindow::updateCustomerStarted(bool state){
    QPixmap customerStateImage("./customer.png");
    QPixmap customerExitImage("./exit.png");
//    ui->customerState->setVisible(true);
    if (state){
        ui->listWidget->addItem("Пришёл новый посетитель");
        ui->customerState->setPixmap(customerStateImage.scaled(100, 100, Qt::KeepAspectRatio));
    }else{
//    ui->listWidget->addItem("Посетитель ушёл, так как мест нет");
        ui->customerState->setPixmap(customerExitImage.scaled(100, 100, Qt::KeepAspectRatio));
    }
//    ui->customerState->setVisible(false);
}

void MainWindow::updateBarber(int state) {
    QPixmap barberSleep("./sleep.png");
    QPixmap barberWork("./work.png");
    QPixmap barberCompl("./compl.png");
    switch (state) {
    case 0:
        ui->listWidget->addItem("Барбер спит");
        ui->barberState->setPixmap(barberSleep.scaled(50, 50, Qt::KeepAspectRatio));
        break;
    case 1:
        ui->listWidget->addItem("Барбер работает");
        ui->barberState->setPixmap(barberWork.scaled(50, 50, Qt::KeepAspectRatio));
        break;
    case 2:
        ui->listWidget->addItem("Барбер завершил работу");
        ui->barberState->setPixmap(barberCompl.scaled(50, 50, Qt::KeepAspectRatio));
        break;
    default:
        break;
    }
}

//emitbarberChanged

void MainWindow::on_customerSlider_valueChanged(int value)
{
    qDebug() << "Slider Customer: " << value;
    customerTask->setSpeedCustomer(value);
}

void MainWindow::on_barberSlider_valueChanged(int value)
{
    qDebug() << "Slider Barber: " << value;
    barberTask->setSpeedBarber(value);
}
